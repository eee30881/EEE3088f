# EEE3088F Project Group 32

![Project Status](https://img.shields.io/badge/Project_Status-Complete-brightgreen?style=for-the-badge) 
![Contributors](https://img.shields.io/badge/Contributors-3-blue?style=for-the-badge) 
![License](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey?style=for-the-badge)

## 📌 Project Status
The project is now **complete**. The first version has gone through the design and testing phase. Below is a summary of the results and learnings from the project.

## 👥 Contributors
The project has three main contributors:  
- [Piwani](https://gitlab.com/txddy-p)  
- [Tokologo](https://gitlab.com/tokologo1234)  
- [Tshepo](https://gitlab.com/tshepolenkoe01)  

Feel free to reach out to us for any questions or collaboration opportunities!



## 📖 Introduction
This project revolves around **light and temperature sensing** using sensors located on a **HAT** connected to an **STM board**. The project has applications in several areas:
- **Outdoor lighting control**: Regulate lights based on ambient light levels.
- **Greenhouse automation**: Regulate irrigation and fans in response to environmental factors like light and temperature.
- **Security systems**: Implement sensors for security lighting.


## 🔧 Technologies & Tools:
![STM32](https://img.shields.io/badge/STM32-blue?style=for-the-badge&logo=stmicroelectronics&logoColor=white) ![C](https://img.shields.io/badge/C-00599C?style=for-the-badge&logo=c&logoColor=white) ![C++](https://img.shields.io/badge/C++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white) ![STM32CubeIDE](https://img.shields.io/badge/STM32CubeIDE-003566?style=for-the-badge&logo=stmicroelectronics&logoColor=white) ![KiCad](https://img.shields.io/badge/KiCad-0033A0?style=for-the-badge&logo=kicad&logoColor=white) ![Git](https://img.shields.io/badge/Git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white)




## ⚙️ Requirements
To replicate or contribute to this project, you will need:
- **STM Discovery board**  
- **KiCad** (for PCB design)  
- **STM32CubeIDE** or **STM32CubeMX** with **VSCode**  



## 🚀 How to Use
The libraries required for interfacing with the digital sensor are located under the `firmware` directory. The `main` function demonstrates how to:
- Read and write to the **EEPROM**
- Interact with the sensor for data acquisition

### 💻 Drivers
The project uses the **CH340C** for USB-to-Serial interfacing.  
👉 Download the drivers for the CH340C from [Sparkfun](https://learn.sparkfun.com/tutorials/how-to-install-ch340-drivers/introduction).

Once installed, the **CH340C** device should be visible in your system for communication.

### 🏃‍♂️ Running the Project
To monitor serial ports and receive data via USB, you'll need a terminal emulator. We used **Putty**, but alternatives such as **Tera Term** or **minicom** should work just fine.



## 📐 PCB Design
The PCB for the project was designed using **KiCad**. Below is an image of the PCB layout:

#### PCB Layout
![PCB Layout](CAD/PCB.jpeg)

#### Putty Terminal Monitoring
![Putty Screenshot](CAD/Putty.jpeg)



## 🔄 Retrospective Review
The project met its main objectives of **sensor communication**, **data storage**, and **USB data transmission**, but there were a few challenges.

### ✅ What Went Well
- Successful communication with sensors and data storage.
- USB-based data display functionality was achieved.

### ⚠️ What Went Wrong
- The **power submodule** didn’t function as intended, and we had to power the board using the **UCT Discovery Board**.
- Some incorrect wiring on the PCB, but nothing fatal to the project’s functionality.

### 🗺️ Board Layout Efficiency
The PCB layout can be optimized for better space usage in future iterations.



## 🤝 Contributing
Pull requests are welcome!  
For major changes, please **open an issue** first to discuss what you'd like to change. Alternatively, you can reach out via email to any of the contributors listed above.

Please ensure that any changes include relevant tests and documentation updates.



## 📜 License
This project is licensed under the [Creative Commons Attribution 4.0 International License](https://choosealicense.com/licenses/cc-by-4.0/).
